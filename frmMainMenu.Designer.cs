﻿namespace SOS_Management_System
{
    partial class frmMainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainMenu));
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            this.studentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrationToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.studentDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.internalMarksEntryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hostelersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.busHoldersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrationFormToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.busFeePaymentToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.feePaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hostelFeesPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scholarshipPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeSalaryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.othersTransactionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentRegistrationRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentRecordToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.studentAttendanceRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.feePaymentRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scholarshipPaymentRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.hostelersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.hostelFeePaymentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.busHoldersToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.busFeePaymentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.employeeRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.employeePaymentRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.otherTransactionRecordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentsRegistrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.internalMarksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.studentProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.feePaymentToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.feeReceiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scholarshipPaymentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.scholarshipPaymentReceiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feesDetailsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.hostlersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hostelFeePaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hostelFeePaymentReceiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.busHoldersToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.busFeePaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.busFeePaymentReceiptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transportationChargesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.employeeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.salaryPaymentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salarySlipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.registrationToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.othersTransactionToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.subjectInfoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.calculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notepadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taskManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mSWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wordpadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Master_entryMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.courseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.departmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eventToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.feesDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hostelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scholarshipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subjectInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.semesterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transportationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.contactUsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.User = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.Time = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Logout = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.studentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.attendanceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.internalMarksEntryToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.feePaymentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.employeeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salaryPaymentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userRegistrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UserType = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup2 = new DevExpress.XtraEditors.TileGroup();
            this.tileItem1 = new DevExpress.XtraEditors.TileItem();
            this.tileItem2 = new DevExpress.XtraEditors.TileItem();
            this.tileItem3 = new DevExpress.XtraEditors.TileItem();
            this.tileItem4 = new DevExpress.XtraEditors.TileItem();
            this.tileGroup3 = new DevExpress.XtraEditors.TileGroup();
            this.tileItem5 = new DevExpress.XtraEditors.TileItem();
            this.tileItem6 = new DevExpress.XtraEditors.TileItem();
            this.tileGroup1 = new DevExpress.XtraEditors.TileGroup();
            this.tileItem7 = new DevExpress.XtraEditors.TileItem();
            this.tileItem8 = new DevExpress.XtraEditors.TileItem();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.tileItem9 = new DevExpress.XtraEditors.TileItem();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // studentToolStripMenuItem
            // 
            this.studentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrationToolStripMenuItem2,
            this.studentDetailsToolStripMenuItem,
            this.internalMarksEntryToolStripMenuItem,
            this.hostelersToolStripMenuItem,
            this.busHoldersToolStripMenuItem,
            this.attendanceToolStripMenuItem,
            this.registrationFormToolStripMenuItem});
            this.studentToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.studentToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.studentToolStripMenuItem.Name = "studentToolStripMenuItem";
            this.studentToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.studentToolStripMenuItem.Text = "Student";
            this.studentToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.studentToolStripMenuItem.Click += new System.EventHandler(this.studentToolStripMenuItem_Click);
            // 
            // registrationToolStripMenuItem2
            // 
            this.registrationToolStripMenuItem2.Image = global::SOS_Management_System.Properties.Resources.images2;
            this.registrationToolStripMenuItem2.Name = "registrationToolStripMenuItem2";
            this.registrationToolStripMenuItem2.Size = new System.Drawing.Size(239, 22);
            this.registrationToolStripMenuItem2.Text = "Registration";
            this.registrationToolStripMenuItem2.Click += new System.EventHandler(this.registrationToolStripMenuItem2_Click);
            // 
            // studentDetailsToolStripMenuItem
            // 
            this.studentDetailsToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.User_icon;
            this.studentDetailsToolStripMenuItem.Name = "studentDetailsToolStripMenuItem";
            this.studentDetailsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.studentDetailsToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.studentDetailsToolStripMenuItem.Text = "Profile Entry";
            this.studentDetailsToolStripMenuItem.Click += new System.EventHandler(this.studentDetailsToolStripMenuItem_Click);
            // 
            // internalMarksEntryToolStripMenuItem
            // 
            this.internalMarksEntryToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.url;
            this.internalMarksEntryToolStripMenuItem.Name = "internalMarksEntryToolStripMenuItem";
            this.internalMarksEntryToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.I)));
            this.internalMarksEntryToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.internalMarksEntryToolStripMenuItem.Text = "Internal Marks Entry";
            this.internalMarksEntryToolStripMenuItem.Click += new System.EventHandler(this.internalMarksEntryToolStripMenuItem_Click);
            // 
            // hostelersToolStripMenuItem
            // 
            this.hostelersToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.images4;
            this.hostelersToolStripMenuItem.Name = "hostelersToolStripMenuItem";
            this.hostelersToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.hostelersToolStripMenuItem.Text = "Hostelers";
            this.hostelersToolStripMenuItem.Click += new System.EventHandler(this.hostelersToolStripMenuItem_Click);
            // 
            // busHoldersToolStripMenuItem
            // 
            this.busHoldersToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.images5;
            this.busHoldersToolStripMenuItem.Name = "busHoldersToolStripMenuItem";
            this.busHoldersToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.busHoldersToolStripMenuItem.Text = "Bus Holders";
            this.busHoldersToolStripMenuItem.Click += new System.EventHandler(this.busHoldersToolStripMenuItem_Click);
            // 
            // attendanceToolStripMenuItem
            // 
            this.attendanceToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.attendance_list_icon;
            this.attendanceToolStripMenuItem.Name = "attendanceToolStripMenuItem";
            this.attendanceToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.attendanceToolStripMenuItem.Text = "Attendance";
            this.attendanceToolStripMenuItem.Click += new System.EventHandler(this.attendanceToolStripMenuItem_Click);
            // 
            // registrationFormToolStripMenuItem
            // 
            this.registrationFormToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.RegistrationIcon1;
            this.registrationFormToolStripMenuItem.Name = "registrationFormToolStripMenuItem";
            this.registrationFormToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.registrationFormToolStripMenuItem.Text = "Registration Form";
            this.registrationFormToolStripMenuItem.Click += new System.EventHandler(this.registrationFormToolStripMenuItem_Click);
            // 
            // employeeToolStripMenuItem
            // 
            this.employeeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.employeeProfileToolStripMenuItem});
            this.employeeToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.employeeToolStripMenuItem.Name = "employeeToolStripMenuItem";
            this.employeeToolStripMenuItem.Size = new System.Drawing.Size(79, 21);
            this.employeeToolStripMenuItem.Text = "Employee";
            // 
            // employeeProfileToolStripMenuItem
            // 
            this.employeeProfileToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.teacher_icon;
            this.employeeProfileToolStripMenuItem.Name = "employeeProfileToolStripMenuItem";
            this.employeeProfileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.E)));
            this.employeeProfileToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.employeeProfileToolStripMenuItem.Text = "Profile Entry";
            this.employeeProfileToolStripMenuItem.Click += new System.EventHandler(this.employeeProfileToolStripMenuItem_Click);
            // 
            // transactionToolStripMenuItem
            // 
            this.transactionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.busFeePaymentToolStripMenuItem2,
            this.feePaymentToolStripMenuItem,
            this.hostelFeesPaymentToolStripMenuItem,
            this.scholarshipPaymentToolStripMenuItem,
            this.employeeSalaryToolStripMenuItem,
            this.othersTransactionToolStripMenuItem});
            this.transactionToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.transactionToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.transactionToolStripMenuItem.Name = "transactionToolStripMenuItem";
            this.transactionToolStripMenuItem.Size = new System.Drawing.Size(89, 21);
            this.transactionToolStripMenuItem.Text = "Transaction";
            // 
            // busFeePaymentToolStripMenuItem2
            // 
            this.busFeePaymentToolStripMenuItem2.Image = global::SOS_Management_System.Properties.Resources._003;
            this.busFeePaymentToolStripMenuItem2.Name = "busFeePaymentToolStripMenuItem2";
            this.busFeePaymentToolStripMenuItem2.Size = new System.Drawing.Size(234, 22);
            this.busFeePaymentToolStripMenuItem2.Text = "Bus Fee Payment";
            this.busFeePaymentToolStripMenuItem2.Click += new System.EventHandler(this.busFeePaymentToolStripMenuItem2_Click);
            // 
            // feePaymentToolStripMenuItem
            // 
            this.feePaymentToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.images;
            this.feePaymentToolStripMenuItem.Name = "feePaymentToolStripMenuItem";
            this.feePaymentToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.feePaymentToolStripMenuItem.Text = "Course Fee Payment";
            this.feePaymentToolStripMenuItem.Click += new System.EventHandler(this.feePaymentToolStripMenuItem_Click);
            // 
            // hostelFeesPaymentToolStripMenuItem
            // 
            this.hostelFeesPaymentToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.transaction_mgmt_icon;
            this.hostelFeesPaymentToolStripMenuItem.Name = "hostelFeesPaymentToolStripMenuItem";
            this.hostelFeesPaymentToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.hostelFeesPaymentToolStripMenuItem.Text = "Hostel Fee Payment";
            this.hostelFeesPaymentToolStripMenuItem.Click += new System.EventHandler(this.hostelFeesPaymentToolStripMenuItem_Click);
            // 
            // scholarshipPaymentToolStripMenuItem
            // 
            this.scholarshipPaymentToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.img_transaction_icon;
            this.scholarshipPaymentToolStripMenuItem.Name = "scholarshipPaymentToolStripMenuItem";
            this.scholarshipPaymentToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.scholarshipPaymentToolStripMenuItem.Text = "Scholarship Payment";
            this.scholarshipPaymentToolStripMenuItem.Click += new System.EventHandler(this.scholarshipPaymentToolStripMenuItem_Click);
            // 
            // employeeSalaryToolStripMenuItem
            // 
            this.employeeSalaryToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.Salaries_sml;
            this.employeeSalaryToolStripMenuItem.Name = "employeeSalaryToolStripMenuItem";
            this.employeeSalaryToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.employeeSalaryToolStripMenuItem.Text = "Employee Salary Payment";
            this.employeeSalaryToolStripMenuItem.Click += new System.EventHandler(this.employeeSalaryToolStripMenuItem_Click);
            // 
            // othersTransactionToolStripMenuItem
            // 
            this.othersTransactionToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.images3;
            this.othersTransactionToolStripMenuItem.Name = "othersTransactionToolStripMenuItem";
            this.othersTransactionToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.othersTransactionToolStripMenuItem.Text = "Others Transaction";
            this.othersTransactionToolStripMenuItem.Click += new System.EventHandler(this.othersTransactionToolStripMenuItem_Click);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.studentRegistrationRecordToolStripMenuItem,
            this.studentRecordToolStripMenuItem1,
            this.studentAttendanceRecordToolStripMenuItem,
            this.toolStripSeparator1,
            this.feePaymentRecordToolStripMenuItem,
            this.scholarshipPaymentRecordToolStripMenuItem,
            this.toolStripSeparator2,
            this.hostelersToolStripMenuItem1,
            this.hostelFeePaymentToolStripMenuItem1,
            this.toolStripSeparator3,
            this.busHoldersToolStripMenuItem1,
            this.busFeePaymentToolStripMenuItem1,
            this.toolStripSeparator4,
            this.employeeRecordToolStripMenuItem,
            this.employeePaymentRecordToolStripMenuItem,
            this.toolStripSeparator5,
            this.otherTransactionRecordToolStripMenuItem});
            this.searchToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.searchToolStripMenuItem.Text = "Records";
            // 
            // studentRegistrationRecordToolStripMenuItem
            // 
            this.studentRegistrationRecordToolStripMenuItem.Name = "studentRegistrationRecordToolStripMenuItem";
            this.studentRegistrationRecordToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.studentRegistrationRecordToolStripMenuItem.Text = "Students Registration";
            this.studentRegistrationRecordToolStripMenuItem.Click += new System.EventHandler(this.studentRegistrationRecordToolStripMenuItem_Click);
            // 
            // studentRecordToolStripMenuItem1
            // 
            this.studentRecordToolStripMenuItem1.Name = "studentRecordToolStripMenuItem1";
            this.studentRecordToolStripMenuItem1.Size = new System.Drawing.Size(207, 22);
            this.studentRecordToolStripMenuItem1.Text = "Students Profile";
            this.studentRecordToolStripMenuItem1.Click += new System.EventHandler(this.studentRecordToolStripMenuItem1_Click);
            // 
            // studentAttendanceRecordToolStripMenuItem
            // 
            this.studentAttendanceRecordToolStripMenuItem.Name = "studentAttendanceRecordToolStripMenuItem";
            this.studentAttendanceRecordToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.studentAttendanceRecordToolStripMenuItem.Text = "Students Attendance";
            this.studentAttendanceRecordToolStripMenuItem.Click += new System.EventHandler(this.studentAttendanceRecordToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(204, 6);
            // 
            // feePaymentRecordToolStripMenuItem
            // 
            this.feePaymentRecordToolStripMenuItem.Name = "feePaymentRecordToolStripMenuItem";
            this.feePaymentRecordToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.feePaymentRecordToolStripMenuItem.Text = "Course Fee Payment";
            this.feePaymentRecordToolStripMenuItem.Click += new System.EventHandler(this.feePaymentRecordToolStripMenuItem_Click);
            // 
            // scholarshipPaymentRecordToolStripMenuItem
            // 
            this.scholarshipPaymentRecordToolStripMenuItem.Name = "scholarshipPaymentRecordToolStripMenuItem";
            this.scholarshipPaymentRecordToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.scholarshipPaymentRecordToolStripMenuItem.Text = "Scholarship Payment";
            this.scholarshipPaymentRecordToolStripMenuItem.Click += new System.EventHandler(this.scholarshipPaymentRecordToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(204, 6);
            // 
            // hostelersToolStripMenuItem1
            // 
            this.hostelersToolStripMenuItem1.Name = "hostelersToolStripMenuItem1";
            this.hostelersToolStripMenuItem1.Size = new System.Drawing.Size(207, 22);
            this.hostelersToolStripMenuItem1.Text = "Hostelers";
            this.hostelersToolStripMenuItem1.Click += new System.EventHandler(this.hostelersToolStripMenuItem1_Click);
            // 
            // hostelFeePaymentToolStripMenuItem1
            // 
            this.hostelFeePaymentToolStripMenuItem1.Name = "hostelFeePaymentToolStripMenuItem1";
            this.hostelFeePaymentToolStripMenuItem1.Size = new System.Drawing.Size(207, 22);
            this.hostelFeePaymentToolStripMenuItem1.Text = "Hostel Fee Payment";
            this.hostelFeePaymentToolStripMenuItem1.Click += new System.EventHandler(this.hostelFeePaymentToolStripMenuItem1_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(204, 6);
            // 
            // busHoldersToolStripMenuItem1
            // 
            this.busHoldersToolStripMenuItem1.Name = "busHoldersToolStripMenuItem1";
            this.busHoldersToolStripMenuItem1.Size = new System.Drawing.Size(207, 22);
            this.busHoldersToolStripMenuItem1.Text = "Bus Holders";
            this.busHoldersToolStripMenuItem1.Click += new System.EventHandler(this.busHoldersToolStripMenuItem1_Click);
            // 
            // busFeePaymentToolStripMenuItem1
            // 
            this.busFeePaymentToolStripMenuItem1.Name = "busFeePaymentToolStripMenuItem1";
            this.busFeePaymentToolStripMenuItem1.Size = new System.Drawing.Size(207, 22);
            this.busFeePaymentToolStripMenuItem1.Text = "Bus Fee Payment";
            this.busFeePaymentToolStripMenuItem1.Click += new System.EventHandler(this.busFeePaymentToolStripMenuItem1_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(204, 6);
            // 
            // employeeRecordToolStripMenuItem
            // 
            this.employeeRecordToolStripMenuItem.Name = "employeeRecordToolStripMenuItem";
            this.employeeRecordToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.employeeRecordToolStripMenuItem.Text = "Employees";
            this.employeeRecordToolStripMenuItem.Click += new System.EventHandler(this.employeeRecordToolStripMenuItem_Click);
            // 
            // employeePaymentRecordToolStripMenuItem
            // 
            this.employeePaymentRecordToolStripMenuItem.Name = "employeePaymentRecordToolStripMenuItem";
            this.employeePaymentRecordToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.employeePaymentRecordToolStripMenuItem.Text = "Employee Payment";
            this.employeePaymentRecordToolStripMenuItem.Click += new System.EventHandler(this.employeePaymentRecordToolStripMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(204, 6);
            // 
            // otherTransactionRecordToolStripMenuItem
            // 
            this.otherTransactionRecordToolStripMenuItem.Name = "otherTransactionRecordToolStripMenuItem";
            this.otherTransactionRecordToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.otherTransactionRecordToolStripMenuItem.Text = "Others Transaction";
            this.otherTransactionRecordToolStripMenuItem.Click += new System.EventHandler(this.otherTransactionRecordToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.studentsToolStripMenuItem,
            this.studentsRegistrationToolStripMenuItem,
            this.attendanceToolStripMenuItem2,
            this.internalMarksToolStripMenuItem,
            this.studentProfileToolStripMenuItem,
            this.toolStripSeparator6,
            this.feePaymentToolStripMenuItem2,
            this.feeReceiptToolStripMenuItem,
            this.scholarshipPaymentToolStripMenuItem1,
            this.scholarshipPaymentReceiptToolStripMenuItem,
            this.feesDetailsToolStripMenuItem1,
            this.toolStripSeparator7,
            this.hostlersToolStripMenuItem,
            this.hostelFeePaymentToolStripMenuItem,
            this.hostelFeePaymentReceiptToolStripMenuItem,
            this.toolStripSeparator8,
            this.busHoldersToolStripMenuItem2,
            this.busFeePaymentToolStripMenuItem,
            this.busFeePaymentReceiptToolStripMenuItem,
            this.transportationChargesToolStripMenuItem,
            this.toolStripSeparator9,
            this.employeeToolStripMenuItem2,
            this.salaryPaymentToolStripMenuItem1,
            this.salarySlipToolStripMenuItem,
            this.toolStripSeparator10,
            this.registrationToolStripMenuItem1,
            this.othersTransactionToolStripMenuItem1,
            this.subjectInfoToolStripMenuItem1});
            this.reportToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(67, 21);
            this.reportToolStripMenuItem.Text = "Reports";
            // 
            // studentsToolStripMenuItem
            // 
            this.studentsToolStripMenuItem.Name = "studentsToolStripMenuItem";
            this.studentsToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.studentsToolStripMenuItem.Text = "Students List";
            this.studentsToolStripMenuItem.Click += new System.EventHandler(this.studentsToolStripMenuItem_Click);
            // 
            // studentsRegistrationToolStripMenuItem
            // 
            this.studentsRegistrationToolStripMenuItem.Name = "studentsRegistrationToolStripMenuItem";
            this.studentsRegistrationToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.studentsRegistrationToolStripMenuItem.Text = "Students Registration";
            this.studentsRegistrationToolStripMenuItem.Click += new System.EventHandler(this.studentsRegistrationToolStripMenuItem_Click);
            // 
            // attendanceToolStripMenuItem2
            // 
            this.attendanceToolStripMenuItem2.Name = "attendanceToolStripMenuItem2";
            this.attendanceToolStripMenuItem2.Size = new System.Drawing.Size(251, 22);
            this.attendanceToolStripMenuItem2.Text = "Students Attendance";
            this.attendanceToolStripMenuItem2.Click += new System.EventHandler(this.attendanceToolStripMenuItem2_Click);
            // 
            // internalMarksToolStripMenuItem
            // 
            this.internalMarksToolStripMenuItem.Name = "internalMarksToolStripMenuItem";
            this.internalMarksToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.internalMarksToolStripMenuItem.Text = "Internal Marks";
            this.internalMarksToolStripMenuItem.Click += new System.EventHandler(this.internalMarksToolStripMenuItem_Click);
            // 
            // studentProfileToolStripMenuItem
            // 
            this.studentProfileToolStripMenuItem.Name = "studentProfileToolStripMenuItem";
            this.studentProfileToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.studentProfileToolStripMenuItem.Text = "Student Profile";
            this.studentProfileToolStripMenuItem.Click += new System.EventHandler(this.studentProfileToolStripMenuItem_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(248, 6);
            // 
            // feePaymentToolStripMenuItem2
            // 
            this.feePaymentToolStripMenuItem2.Name = "feePaymentToolStripMenuItem2";
            this.feePaymentToolStripMenuItem2.Size = new System.Drawing.Size(251, 22);
            this.feePaymentToolStripMenuItem2.Text = "Course Fee Payment";
            this.feePaymentToolStripMenuItem2.Click += new System.EventHandler(this.feePaymentToolStripMenuItem2_Click);
            // 
            // feeReceiptToolStripMenuItem
            // 
            this.feeReceiptToolStripMenuItem.Name = "feeReceiptToolStripMenuItem";
            this.feeReceiptToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.feeReceiptToolStripMenuItem.Text = "Course Fee Payment Receipt";
            this.feeReceiptToolStripMenuItem.Click += new System.EventHandler(this.feeReceiptToolStripMenuItem_Click);
            // 
            // scholarshipPaymentToolStripMenuItem1
            // 
            this.scholarshipPaymentToolStripMenuItem1.Name = "scholarshipPaymentToolStripMenuItem1";
            this.scholarshipPaymentToolStripMenuItem1.Size = new System.Drawing.Size(251, 22);
            this.scholarshipPaymentToolStripMenuItem1.Text = "Scholarship Payment";
            this.scholarshipPaymentToolStripMenuItem1.Click += new System.EventHandler(this.scholarshipPaymentToolStripMenuItem1_Click);
            // 
            // scholarshipPaymentReceiptToolStripMenuItem
            // 
            this.scholarshipPaymentReceiptToolStripMenuItem.Name = "scholarshipPaymentReceiptToolStripMenuItem";
            this.scholarshipPaymentReceiptToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.scholarshipPaymentReceiptToolStripMenuItem.Text = "Scholarship Payment Receipt";
            this.scholarshipPaymentReceiptToolStripMenuItem.Click += new System.EventHandler(this.scholarshipPaymentReceiptToolStripMenuItem_Click);
            // 
            // feesDetailsToolStripMenuItem1
            // 
            this.feesDetailsToolStripMenuItem1.Name = "feesDetailsToolStripMenuItem1";
            this.feesDetailsToolStripMenuItem1.Size = new System.Drawing.Size(251, 22);
            this.feesDetailsToolStripMenuItem1.Text = "Fees Details";
            this.feesDetailsToolStripMenuItem1.Click += new System.EventHandler(this.feesDetailsToolStripMenuItem1_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(248, 6);
            // 
            // hostlersToolStripMenuItem
            // 
            this.hostlersToolStripMenuItem.Name = "hostlersToolStripMenuItem";
            this.hostlersToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.hostlersToolStripMenuItem.Text = "Hostelers";
            this.hostlersToolStripMenuItem.Click += new System.EventHandler(this.hostlersToolStripMenuItem_Click);
            // 
            // hostelFeePaymentToolStripMenuItem
            // 
            this.hostelFeePaymentToolStripMenuItem.Name = "hostelFeePaymentToolStripMenuItem";
            this.hostelFeePaymentToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.hostelFeePaymentToolStripMenuItem.Text = "Hostel Fee Payment";
            this.hostelFeePaymentToolStripMenuItem.Click += new System.EventHandler(this.hostelFeePaymentToolStripMenuItem_Click);
            // 
            // hostelFeePaymentReceiptToolStripMenuItem
            // 
            this.hostelFeePaymentReceiptToolStripMenuItem.Name = "hostelFeePaymentReceiptToolStripMenuItem";
            this.hostelFeePaymentReceiptToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.hostelFeePaymentReceiptToolStripMenuItem.Text = "Hostel Fee Payment Receipt";
            this.hostelFeePaymentReceiptToolStripMenuItem.Click += new System.EventHandler(this.hostelFeePaymentReceiptToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(248, 6);
            // 
            // busHoldersToolStripMenuItem2
            // 
            this.busHoldersToolStripMenuItem2.Name = "busHoldersToolStripMenuItem2";
            this.busHoldersToolStripMenuItem2.Size = new System.Drawing.Size(251, 22);
            this.busHoldersToolStripMenuItem2.Text = "Bus Holders";
            this.busHoldersToolStripMenuItem2.Click += new System.EventHandler(this.busHoldersToolStripMenuItem2_Click);
            // 
            // busFeePaymentToolStripMenuItem
            // 
            this.busFeePaymentToolStripMenuItem.Name = "busFeePaymentToolStripMenuItem";
            this.busFeePaymentToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.busFeePaymentToolStripMenuItem.Text = "Bus Fee Payment";
            this.busFeePaymentToolStripMenuItem.Click += new System.EventHandler(this.busFeePaymentToolStripMenuItem_Click);
            // 
            // busFeePaymentReceiptToolStripMenuItem
            // 
            this.busFeePaymentReceiptToolStripMenuItem.Name = "busFeePaymentReceiptToolStripMenuItem";
            this.busFeePaymentReceiptToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.busFeePaymentReceiptToolStripMenuItem.Text = "Bus Fee Payment Receipt";
            this.busFeePaymentReceiptToolStripMenuItem.Click += new System.EventHandler(this.busFeePaymentReceiptToolStripMenuItem_Click);
            // 
            // transportationChargesToolStripMenuItem
            // 
            this.transportationChargesToolStripMenuItem.Name = "transportationChargesToolStripMenuItem";
            this.transportationChargesToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.transportationChargesToolStripMenuItem.Text = "Transportation Charges";
            this.transportationChargesToolStripMenuItem.Click += new System.EventHandler(this.transportationChargesToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(248, 6);
            // 
            // employeeToolStripMenuItem2
            // 
            this.employeeToolStripMenuItem2.Name = "employeeToolStripMenuItem2";
            this.employeeToolStripMenuItem2.Size = new System.Drawing.Size(251, 22);
            this.employeeToolStripMenuItem2.Text = "Employees";
            this.employeeToolStripMenuItem2.Click += new System.EventHandler(this.employeeToolStripMenuItem2_Click);
            // 
            // salaryPaymentToolStripMenuItem1
            // 
            this.salaryPaymentToolStripMenuItem1.Name = "salaryPaymentToolStripMenuItem1";
            this.salaryPaymentToolStripMenuItem1.Size = new System.Drawing.Size(251, 22);
            this.salaryPaymentToolStripMenuItem1.Text = "Salary Payment";
            this.salaryPaymentToolStripMenuItem1.Click += new System.EventHandler(this.salaryPaymentToolStripMenuItem1_Click);
            // 
            // salarySlipToolStripMenuItem
            // 
            this.salarySlipToolStripMenuItem.Name = "salarySlipToolStripMenuItem";
            this.salarySlipToolStripMenuItem.Size = new System.Drawing.Size(251, 22);
            this.salarySlipToolStripMenuItem.Text = "Salary Slip";
            this.salarySlipToolStripMenuItem.Click += new System.EventHandler(this.salarySlipToolStripMenuItem_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(248, 6);
            // 
            // registrationToolStripMenuItem1
            // 
            this.registrationToolStripMenuItem1.Name = "registrationToolStripMenuItem1";
            this.registrationToolStripMenuItem1.Size = new System.Drawing.Size(251, 22);
            this.registrationToolStripMenuItem1.Text = "Users Registration";
            this.registrationToolStripMenuItem1.Click += new System.EventHandler(this.registrationToolStripMenuItem1_Click);
            // 
            // othersTransactionToolStripMenuItem1
            // 
            this.othersTransactionToolStripMenuItem1.Name = "othersTransactionToolStripMenuItem1";
            this.othersTransactionToolStripMenuItem1.Size = new System.Drawing.Size(251, 22);
            this.othersTransactionToolStripMenuItem1.Text = "Others Transaction";
            this.othersTransactionToolStripMenuItem1.Click += new System.EventHandler(this.othersTransactionToolStripMenuItem1_Click);
            // 
            // subjectInfoToolStripMenuItem1
            // 
            this.subjectInfoToolStripMenuItem1.Name = "subjectInfoToolStripMenuItem1";
            this.subjectInfoToolStripMenuItem1.Size = new System.Drawing.Size(251, 22);
            this.subjectInfoToolStripMenuItem1.Text = "Subject Info";
            this.subjectInfoToolStripMenuItem1.Click += new System.EventHandler(this.subjectInfoToolStripMenuItem1_Click);
            // 
            // toolsMenu
            // 
            this.toolsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculatorToolStripMenuItem,
            this.notepadToolStripMenuItem,
            this.taskManagerToolStripMenuItem,
            this.mSWordToolStripMenuItem,
            this.wordpadToolStripMenuItem});
            this.toolsMenu.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolsMenu.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.toolsMenu.Name = "toolsMenu";
            this.toolsMenu.Size = new System.Drawing.Size(51, 21);
            this.toolsMenu.Text = "&Tools";
            // 
            // calculatorToolStripMenuItem
            // 
            this.calculatorToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.calc;
            this.calculatorToolStripMenuItem.Name = "calculatorToolStripMenuItem";
            this.calculatorToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.calculatorToolStripMenuItem.Text = "Calculator";
            this.calculatorToolStripMenuItem.Click += new System.EventHandler(this.calculatorToolStripMenuItem_Click);
            // 
            // notepadToolStripMenuItem
            // 
            this.notepadToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.Notepad1;
            this.notepadToolStripMenuItem.Name = "notepadToolStripMenuItem";
            this.notepadToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.notepadToolStripMenuItem.Text = "Notepad";
            this.notepadToolStripMenuItem.Click += new System.EventHandler(this.notepadToolStripMenuItem_Click);
            // 
            // taskManagerToolStripMenuItem
            // 
            this.taskManagerToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.task_manager1;
            this.taskManagerToolStripMenuItem.Name = "taskManagerToolStripMenuItem";
            this.taskManagerToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.taskManagerToolStripMenuItem.Text = "Task Manager";
            this.taskManagerToolStripMenuItem.Click += new System.EventHandler(this.taskManagerToolStripMenuItem_Click);
            // 
            // mSWordToolStripMenuItem
            // 
            this.mSWordToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.MS_Word_2_icon1;
            this.mSWordToolStripMenuItem.Name = "mSWordToolStripMenuItem";
            this.mSWordToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.mSWordToolStripMenuItem.Text = "MS Word";
            this.mSWordToolStripMenuItem.Click += new System.EventHandler(this.mSWordToolStripMenuItem_Click);
            // 
            // wordpadToolStripMenuItem
            // 
            this.wordpadToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.Wordpad_icon__Windows_7_1;
            this.wordpadToolStripMenuItem.Name = "wordpadToolStripMenuItem";
            this.wordpadToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.wordpadToolStripMenuItem.Text = "Wordpad";
            this.wordpadToolStripMenuItem.Click += new System.EventHandler(this.wordpadToolStripMenuItem_Click);
            // 
            // usersToolStripMenuItem
            // 
            this.usersToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loginDetailsToolStripMenuItem,
            this.registrationToolStripMenuItem});
            this.usersToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usersToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.usersToolStripMenuItem.Name = "usersToolStripMenuItem";
            this.usersToolStripMenuItem.Size = new System.Drawing.Size(53, 21);
            this.usersToolStripMenuItem.Text = "&Users";
            // 
            // loginDetailsToolStripMenuItem
            // 
            this.loginDetailsToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.Log_Details;
            this.loginDetailsToolStripMenuItem.Name = "loginDetailsToolStripMenuItem";
            this.loginDetailsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.L)));
            this.loginDetailsToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.loginDetailsToolStripMenuItem.Text = "Login Details";
            this.loginDetailsToolStripMenuItem.Click += new System.EventHandler(this.loginDetailsToolStripMenuItem_Click);
            // 
            // registrationToolStripMenuItem
            // 
            this.registrationToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.registration_icon_up;
            this.registrationToolStripMenuItem.Name = "registrationToolStripMenuItem";
            this.registrationToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.R)));
            this.registrationToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.registrationToolStripMenuItem.Text = "Registration";
            this.registrationToolStripMenuItem.Click += new System.EventHandler(this.registrationToolStripMenuItem_Click);
            // 
            // Master_entryMenu
            // 
            this.Master_entryMenu.AutoToolTip = true;
            this.Master_entryMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.courseToolStripMenuItem,
            this.departmentToolStripMenuItem,
            this.eventToolStripMenuItem,
            this.feesDetailsToolStripMenuItem,
            this.hostelToolStripMenuItem,
            this.scholarshipToolStripMenuItem,
            this.subjectInfoToolStripMenuItem,
            this.semesterToolStripMenuItem,
            this.sectionToolStripMenuItem,
            this.transportationToolStripMenuItem});
            this.Master_entryMenu.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Master_entryMenu.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Master_entryMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.Master_entryMenu.Name = "Master_entryMenu";
            this.Master_entryMenu.Size = new System.Drawing.Size(69, 21);
            this.Master_entryMenu.Text = "&Settings";
            this.Master_entryMenu.ToolTipText = "System Settings ";
            // 
            // courseToolStripMenuItem
            // 
            this.courseToolStripMenuItem.Name = "courseToolStripMenuItem";
            this.courseToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.C)));
            this.courseToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.courseToolStripMenuItem.Text = "Course";
            this.courseToolStripMenuItem.Click += new System.EventHandler(this.courseToolStripMenuItem_Click);
            // 
            // departmentToolStripMenuItem
            // 
            this.departmentToolStripMenuItem.Name = "departmentToolStripMenuItem";
            this.departmentToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.D)));
            this.departmentToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.departmentToolStripMenuItem.Text = "Department";
            this.departmentToolStripMenuItem.Click += new System.EventHandler(this.departmentToolStripMenuItem_Click);
            // 
            // eventToolStripMenuItem
            // 
            this.eventToolStripMenuItem.Name = "eventToolStripMenuItem";
            this.eventToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.E)));
            this.eventToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.eventToolStripMenuItem.Text = "Event";
            this.eventToolStripMenuItem.Click += new System.EventHandler(this.eventToolStripMenuItem_Click);
            // 
            // feesDetailsToolStripMenuItem
            // 
            this.feesDetailsToolStripMenuItem.Name = "feesDetailsToolStripMenuItem";
            this.feesDetailsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F)));
            this.feesDetailsToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.feesDetailsToolStripMenuItem.Text = "Fees Details";
            this.feesDetailsToolStripMenuItem.Click += new System.EventHandler(this.feesDetailsToolStripMenuItem_Click);
            // 
            // hostelToolStripMenuItem
            // 
            this.hostelToolStripMenuItem.Name = "hostelToolStripMenuItem";
            this.hostelToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.H)));
            this.hostelToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.hostelToolStripMenuItem.Text = "Hostel";
            this.hostelToolStripMenuItem.Click += new System.EventHandler(this.hostelToolStripMenuItem_Click);
            // 
            // scholarshipToolStripMenuItem
            // 
            this.scholarshipToolStripMenuItem.Name = "scholarshipToolStripMenuItem";
            this.scholarshipToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.scholarshipToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.scholarshipToolStripMenuItem.Text = "Scholarship";
            this.scholarshipToolStripMenuItem.Click += new System.EventHandler(this.scholarshipToolStripMenuItem_Click);
            // 
            // subjectInfoToolStripMenuItem
            // 
            this.subjectInfoToolStripMenuItem.Name = "subjectInfoToolStripMenuItem";
            this.subjectInfoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.S)));
            this.subjectInfoToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.subjectInfoToolStripMenuItem.Text = "Subject Info";
            this.subjectInfoToolStripMenuItem.Click += new System.EventHandler(this.subjectInfoToolStripMenuItem_Click);
            // 
            // semesterToolStripMenuItem
            // 
            this.semesterToolStripMenuItem.Name = "semesterToolStripMenuItem";
            this.semesterToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.semesterToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.semesterToolStripMenuItem.Text = "Semester";
            this.semesterToolStripMenuItem.Click += new System.EventHandler(this.semesterToolStripMenuItem_Click);
            // 
            // sectionToolStripMenuItem
            // 
            this.sectionToolStripMenuItem.Name = "sectionToolStripMenuItem";
            this.sectionToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.sectionToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.sectionToolStripMenuItem.Text = "Section";
            this.sectionToolStripMenuItem.Click += new System.EventHandler(this.sectionToolStripMenuItem_Click);
            // 
            // transportationToolStripMenuItem
            // 
            this.transportationToolStripMenuItem.Name = "transportationToolStripMenuItem";
            this.transportationToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.T)));
            this.transportationToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.transportationToolStripMenuItem.Text = "Transportation";
            this.transportationToolStripMenuItem.Click += new System.EventHandler(this.transportationToolStripMenuItem_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contactUsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpMenu.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpMenu.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(48, 21);
            this.helpMenu.Text = "&Help";
            // 
            // contactUsToolStripMenuItem
            // 
            this.contactUsToolStripMenuItem.Name = "contactUsToolStripMenuItem";
            this.contactUsToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.contactUsToolStripMenuItem.Text = "Contact";
            this.contactUsToolStripMenuItem.Click += new System.EventHandler(this.contactUsToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.User,
            this.toolStripStatusLabel3,
            this.Time});
            this.statusStrip.Location = new System.Drawing.Point(0, 511);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(914, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(80, 17);
            this.toolStripStatusLabel1.Text = "Logged in As :";
            // 
            // User
            // 
            this.User.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.User.Image = global::SOS_Management_System.Properties.Resources.User_icon;
            this.User.Name = "User";
            this.User.Size = new System.Drawing.Size(46, 17);
            this.User.Text = "User";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(691, 17);
            this.toolStripStatusLabel3.Spring = true;
            // 
            // Time
            // 
            this.Time.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Time.Image = global::SOS_Management_System.Properties.Resources.time_1920x1200;
            this.Time.Name = "Time";
            this.Time.Size = new System.Drawing.Size(51, 17);
            this.Time.Text = "Time";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Logout
            // 
            this.Logout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Logout.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Logout.Location = new System.Drawing.Point(648, 45);
            this.Logout.Name = "Logout";
            this.Logout.Size = new System.Drawing.Size(80, 28);
            this.Logout.TabIndex = 3;
            this.Logout.Text = "Logout";
            this.Logout.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(175)))), ((int)(((byte)(70)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.studentToolStripMenuItem1,
            this.attendanceToolStripMenuItem1,
            this.internalMarksEntryToolStripMenuItem1,
            this.feePaymentToolStripMenuItem1,
            this.employeeToolStripMenuItem1,
            this.salaryPaymentToolStripMenuItem,
            this.userRegistrationToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 25);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(914, 75);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // studentToolStripMenuItem1
            // 
            this.studentToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("studentToolStripMenuItem1.Image")));
            this.studentToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.studentToolStripMenuItem1.Name = "studentToolStripMenuItem1";
            this.studentToolStripMenuItem1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.studentToolStripMenuItem1.Size = new System.Drawing.Size(64, 71);
            this.studentToolStripMenuItem1.Text = "Student";
            this.studentToolStripMenuItem1.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.studentToolStripMenuItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.studentToolStripMenuItem1.Click += new System.EventHandler(this.studentToolStripMenuItem1_Click);
            // 
            // attendanceToolStripMenuItem1
            // 
            this.attendanceToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendanceToolStripMenuItem1.Image = global::SOS_Management_System.Properties.Resources.attendance_list_icon;
            this.attendanceToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.attendanceToolStripMenuItem1.Name = "attendanceToolStripMenuItem1";
            this.attendanceToolStripMenuItem1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.attendanceToolStripMenuItem1.Size = new System.Drawing.Size(85, 71);
            this.attendanceToolStripMenuItem1.Text = "Attendance";
            this.attendanceToolStripMenuItem1.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.attendanceToolStripMenuItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.attendanceToolStripMenuItem1.Click += new System.EventHandler(this.attendanceToolStripMenuItem1_Click);
            // 
            // internalMarksEntryToolStripMenuItem1
            // 
            this.internalMarksEntryToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.internalMarksEntryToolStripMenuItem1.Image = global::SOS_Management_System.Properties.Resources.url;
            this.internalMarksEntryToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.internalMarksEntryToolStripMenuItem1.Name = "internalMarksEntryToolStripMenuItem1";
            this.internalMarksEntryToolStripMenuItem1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.internalMarksEntryToolStripMenuItem1.Size = new System.Drawing.Size(107, 71);
            this.internalMarksEntryToolStripMenuItem1.Text = "Internal Marks ";
            this.internalMarksEntryToolStripMenuItem1.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.internalMarksEntryToolStripMenuItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.internalMarksEntryToolStripMenuItem1.Click += new System.EventHandler(this.internalMarksEntryToolStripMenuItem1_Click);
            // 
            // feePaymentToolStripMenuItem1
            // 
            this.feePaymentToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.feePaymentToolStripMenuItem1.Image = global::SOS_Management_System.Properties.Resources.images;
            this.feePaymentToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.feePaymentToolStripMenuItem1.Name = "feePaymentToolStripMenuItem1";
            this.feePaymentToolStripMenuItem1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.feePaymentToolStripMenuItem1.Size = new System.Drawing.Size(93, 71);
            this.feePaymentToolStripMenuItem1.Text = "Fee Payment";
            this.feePaymentToolStripMenuItem1.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.feePaymentToolStripMenuItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.feePaymentToolStripMenuItem1.Click += new System.EventHandler(this.feePaymentToolStripMenuItem1_Click);
            // 
            // employeeToolStripMenuItem1
            // 
            this.employeeToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeToolStripMenuItem1.Image = global::SOS_Management_System.Properties.Resources.teacher_icon;
            this.employeeToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.employeeToolStripMenuItem1.Name = "employeeToolStripMenuItem1";
            this.employeeToolStripMenuItem1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.employeeToolStripMenuItem1.Size = new System.Drawing.Size(77, 71);
            this.employeeToolStripMenuItem1.Text = "Employee";
            this.employeeToolStripMenuItem1.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.employeeToolStripMenuItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.employeeToolStripMenuItem1.Click += new System.EventHandler(this.employeeToolStripMenuItem1_Click);
            // 
            // salaryPaymentToolStripMenuItem
            // 
            this.salaryPaymentToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salaryPaymentToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.Salaries_sml;
            this.salaryPaymentToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.salaryPaymentToolStripMenuItem.Name = "salaryPaymentToolStripMenuItem";
            this.salaryPaymentToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.salaryPaymentToolStripMenuItem.Size = new System.Drawing.Size(108, 71);
            this.salaryPaymentToolStripMenuItem.Text = "Salary Payment";
            this.salaryPaymentToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.salaryPaymentToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.salaryPaymentToolStripMenuItem.Click += new System.EventHandler(this.salaryPaymentToolStripMenuItem_Click);
            // 
            // userRegistrationToolStripMenuItem
            // 
            this.userRegistrationToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userRegistrationToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.registration_icon_up;
            this.userRegistrationToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.userRegistrationToolStripMenuItem.Name = "userRegistrationToolStripMenuItem";
            this.userRegistrationToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.userRegistrationToolStripMenuItem.Size = new System.Drawing.Size(90, 71);
            this.userRegistrationToolStripMenuItem.Text = "Registration";
            this.userRegistrationToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.userRegistrationToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.userRegistrationToolStripMenuItem.ToolTipText = "User Registration";
            this.userRegistrationToolStripMenuItem.Click += new System.EventHandler(this.userRegistrationToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logoutToolStripMenuItem.Image = global::SOS_Management_System.Properties.Resources.logout;
            this.logoutToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(62, 71);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.logoutToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // UserType
            // 
            this.UserType.AutoSize = true;
            this.UserType.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.UserType.ForeColor = System.Drawing.Color.White;
            this.UserType.Location = new System.Drawing.Point(12, 529);
            this.UserType.Name = "UserType";
            this.UserType.Size = new System.Drawing.Size(10, 13);
            this.UserType.TabIndex = 8;
            this.UserType.Text = ".";
            this.UserType.Visible = false;
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.studentToolStripMenuItem,
            this.transactionToolStripMenuItem,
            this.employeeToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.reportToolStripMenuItem,
            this.toolsMenu,
            this.Master_entryMenu,
            this.usersToolStripMenuItem,
            this.helpMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip.Size = new System.Drawing.Size(914, 25);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // tileControl1
            // 
            this.tileControl1.AppearanceGroupText.BackColor = System.Drawing.Color.White;
            this.tileControl1.AppearanceGroupText.BackColor2 = System.Drawing.Color.White;
            this.tileControl1.AppearanceGroupText.BorderColor = System.Drawing.Color.White;
            this.tileControl1.AppearanceGroupText.Font = new System.Drawing.Font("Segoe UI Semibold", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tileControl1.AppearanceGroupText.ForeColor = System.Drawing.Color.Goldenrod;
            this.tileControl1.AppearanceGroupText.Options.UseBackColor = true;
            this.tileControl1.AppearanceGroupText.Options.UseBorderColor = true;
            this.tileControl1.AppearanceGroupText.Options.UseFont = true;
            this.tileControl1.AppearanceGroupText.Options.UseForeColor = true;
            this.tileControl1.AppearanceText.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tileControl1.AppearanceText.Options.UseFont = true;
            this.tileControl1.BackgroundImage = global::SOS_Management_System.Properties.Resources.all_schoolbg2;
            this.tileControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup2);
            this.tileControl1.Groups.Add(this.tileGroup3);
            this.tileControl1.Groups.Add(this.tileGroup1);
            this.tileControl1.Groups.Add(this.tileGroup5);
            this.tileControl1.HorizontalContentAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.tileControl1.ItemSize = 150;
            this.tileControl1.Location = new System.Drawing.Point(0, 100);
            this.tileControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.tileControl1.MaxId = 10;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollButtons;
            this.tileControl1.ShowGroupText = true;
            this.tileControl1.Size = new System.Drawing.Size(914, 411);
            this.tileControl1.TabIndex = 10;
            this.tileControl1.Text = "tileControl1";
            this.tileControl1.VerticalContentAlignment = DevExpress.Utils.VertAlignment.Top;
            // 
            // tileGroup2
            // 
            this.tileGroup2.Items.Add(this.tileItem1);
            this.tileGroup2.Items.Add(this.tileItem2);
            this.tileGroup2.Items.Add(this.tileItem3);
            this.tileGroup2.Items.Add(this.tileItem4);
            this.tileGroup2.Name = "tileGroup2";
            this.tileGroup2.Text = "Student";
            // 
            // tileItem1
            // 
            tileItemElement1.Image = ((System.Drawing.Image)(resources.GetObject("tileItemElement1.Image")));
            tileItemElement1.Text = "Student ";
            tileItemElement2.Text = "Profile Entry";
            this.tileItem1.Elements.Add(tileItemElement1);
            this.tileItem1.Elements.Add(tileItemElement2);
            this.tileItem1.Id = 0;
            this.tileItem1.IsLarge = true;
            this.tileItem1.Name = "tileItem1";
            this.tileItem1.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.studentToolStripMenuItem1_Click);
            // 
            // tileItem2
            // 
            tileItemElement3.Image = ((System.Drawing.Image)(resources.GetObject("tileItemElement3.Image")));
            tileItemElement3.Text = "Attendence";
            this.tileItem2.Elements.Add(tileItemElement3);
            this.tileItem2.Id = 1;
            this.tileItem2.Name = "tileItem2";
            this.tileItem2.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.attendanceToolStripMenuItem1_Click);
            // 
            // tileItem3
            // 
            tileItemElement4.Image = ((System.Drawing.Image)(resources.GetObject("tileItemElement4.Image")));
            tileItemElement4.Text = "Internal Marks";
            this.tileItem3.Elements.Add(tileItemElement4);
            this.tileItem3.Id = 2;
            this.tileItem3.Name = "tileItem3";
            this.tileItem3.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.internalMarksEntryToolStripMenuItem1_Click);
            // 
            // tileItem4
            // 
            tileItemElement5.Image = global::SOS_Management_System.Properties.Resources.images;
            tileItemElement5.Text = "Fee Payment";
            this.tileItem4.Elements.Add(tileItemElement5);
            this.tileItem4.Id = 3;
            this.tileItem4.Name = "tileItem4";
            this.tileItem4.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.feePaymentToolStripMenuItem1_Click);
            // 
            // tileGroup3
            // 
            this.tileGroup3.Items.Add(this.tileItem5);
            this.tileGroup3.Items.Add(this.tileItem6);
            this.tileGroup3.Name = "tileGroup3";
            this.tileGroup3.Text = "Employee";
            // 
            // tileItem5
            // 
            tileItemElement6.Image = global::SOS_Management_System.Properties.Resources.images5;
            tileItemElement6.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.Squeeze;
            tileItemElement6.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Left;
            tileItemElement6.Text = "Details";
            tileItemElement7.Text = "Employee";
            this.tileItem5.Elements.Add(tileItemElement6);
            this.tileItem5.Elements.Add(tileItemElement7);
            this.tileItem5.Id = 4;
            this.tileItem5.IsLarge = true;
            this.tileItem5.Name = "tileItem5";
            this.tileItem5.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.employeeToolStripMenuItem1_Click);
            // 
            // tileItem6
            // 
            tileItemElement8.Image = global::SOS_Management_System.Properties.Resources.salary_icon1;
            tileItemElement8.Text = "Salary Payments";
            this.tileItem6.Elements.Add(tileItemElement8);
            this.tileItem6.Id = 5;
            this.tileItem6.Name = "tileItem6";
            this.tileItem6.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.salaryPaymentToolStripMenuItem_Click);
            // 
            // tileGroup1
            // 
            this.tileGroup1.Items.Add(this.tileItem7);
            this.tileGroup1.Items.Add(this.tileItem8);
            this.tileGroup1.Name = "tileGroup1";
            this.tileGroup1.Text = "Reports";
            // 
            // tileItem7
            // 
            tileItemElement9.Text = "Course Fee Payment";
            this.tileItem7.Elements.Add(tileItemElement9);
            this.tileItem7.Id = 7;
            this.tileItem7.Name = "tileItem7";
            this.tileItem7.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem7_ItemClick);
            // 
            // tileItem8
            // 
            tileItemElement10.Text = "Transactions Records";
            this.tileItem8.Elements.Add(tileItemElement10);
            this.tileItem8.Id = 8;
            this.tileItem8.Name = "tileItem8";
            this.tileItem8.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem8_ItemClick);
            // 
            // tileGroup5
            // 
            this.tileGroup5.Items.Add(this.tileItem9);
            this.tileGroup5.Name = "tileGroup5";
            this.tileGroup5.Text = null;
            // 
            // tileItem9
            // 
            tileItemElement11.Image = ((System.Drawing.Image)(resources.GetObject("tileItemElement11.Image")));
            tileItemElement11.Text = "Settings";
            this.tileItem9.Elements.Add(tileItemElement11);
            this.tileItem9.Id = 9;
            this.tileItem9.Name = "tileItem9";
            this.tileItem9.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem9_ItemClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(620, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 25);
            this.label1.TabIndex = 12;
            // 
            // frmMainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(914, 533);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tileControl1);
            this.Controls.Add(this.UserType);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.Logout);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "frmMainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SOS Management System";
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.ToolStripMenuItem Master_entryMenu;
        private System.Windows.Forms.ToolStripMenuItem toolsMenu;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem courseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contactUsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculatorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notepadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loginDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripMenuItem departmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feesDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scholarshipToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem internalMarksEntryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentRecordToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem employeeRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feePaymentRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feePaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeSalaryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scholarshipPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem othersTransactionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scholarshipPaymentRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeePaymentRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otherTransactionRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attendanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentAttendanceRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        public System.Windows.Forms.ToolStripStatusLabel User;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel Time;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button Logout;
        private System.Windows.Forms.ToolStripMenuItem subjectInfoToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem studentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem employeeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem feePaymentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem attendanceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem userRegistrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salaryPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taskManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mSWordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem internalMarksEntryToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem employeeToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem internalMarksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrationToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem salaryPaymentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem feePaymentToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem scholarshipPaymentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem attendanceToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem othersTransactionToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem wordpadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem semesterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sectionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registrationToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem registrationFormToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentRegistrationRecordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentsRegistrationToolStripMenuItem;
        public System.Windows.Forms.Label UserType;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem hostelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hostelFeesPaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hostelersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hostelersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem hostelFeePaymentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem busFeePaymentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem hostlersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem studentProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salarySlipToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hostelFeePaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem busFeePaymentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feeReceiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scholarshipPaymentReceiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem busFeePaymentReceiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hostelFeePaymentReceiptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem feesDetailsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem transportationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transportationChargesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem busFeePaymentToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem busHoldersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem busHoldersToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem busHoldersToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem subjectInfoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem eventToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.MenuStrip menuStrip;
        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup2;
        private DevExpress.XtraEditors.TileItem tileItem1;
        private DevExpress.XtraEditors.TileItem tileItem2;
        private DevExpress.XtraEditors.TileItem tileItem3;
        private DevExpress.XtraEditors.TileItem tileItem4;
        private DevExpress.XtraEditors.TileGroup tileGroup3;
        private DevExpress.XtraEditors.TileItem tileItem5;
        private DevExpress.XtraEditors.TileItem tileItem6;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TileGroup tileGroup1;
        private DevExpress.XtraEditors.TileItem tileItem7;
        private DevExpress.XtraEditors.TileItem tileItem8;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileItem tileItem9;
    }
}



